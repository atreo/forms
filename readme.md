Atreo/Forms
===========================

Installation
------------

```sh
$ composer require atreo/forms
```

Extension configuration:

```
forms: Atreo\Forms\DI\FormsExtension
```

Form definition:

```
use Atreo\Forms\Form;



class MyForm extends Form
{

    public function __construct()
    {
        parent::__construct();

        // anti spam / add's hidden input which filters submitted forms

		$this->enableAntiSpam();

        // date handlers / default format j.n.Y

		$this->addDatePicker('createdAt', 'Date');

		// checkboxList, radioList, select, multiSelect for classes

		$this->addEntityCheckboxList('items', 'Select items', $items, 'id', 'name');
		$this->addEntityRadioList('items', 'Select items', $items, 'id', 'name');
		$this->addEntitySelect('items', 'Select item', $items, 'id', 'name');
		$this->addEntityMultiSelect('items', 'Select items', $items, $size, 'id', 'name');

		// document handlers / opens document manager

		$this->addDocumentsSelector('documents', 'Select documents', $itemsLimit;
		$this->addImagesSelector('documents', 'Select documents', $itemsLimit);

		// image upload

		$this->addImageUpload('image', 'Upload image');

    }

}

```

Form component:

```
protected function createComponentMyForm()
{
    $form = new MyForm();
    $form->onSuccess[] = function(MyForm $form) {
        $values = $form->values;

        // do something

        $this->presenter->successFlashMessage("Děkujeme vám za hodnocení.");
        $this->redirect('this');
    };
    return $form;
}
```


Form mapped to entity:

```
protected function createComponentMyorm()
{
    if ($this->myEntity) {
        $myEntity = $this->myEntity;
    } else {
        $myEntity = new MyEntity();
        $this->em->persist($myEntity);
    }

    $form = new MyForm();
    $form->mapToEntity($myEntity);
    $form->setCustomMapping('isDelayed', function($isDelayed = NULL) use ($myEntity) {
        if ($isDelayed) {
            $myEntity->deliveryTime->modify('+ 1 month');
        }
    });

    $form->onSuccess[] = function(MyForm $form) {
        $this->em->flush();
        $this->presenter->successFlashMessage("MyEntity saved.");
        $this->redirect('this');
    };
    return $form;
}
```


Form rendering:

```
// renders whole form in bootstrap-like style
{control myForm}


// manual render
{form myForm}
    {pair name} // renders label and input with bootstrap-like style

    {label name /} // renders input as usual
    {input name} // renders input as usual
{/form}

```
