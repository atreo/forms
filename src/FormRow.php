<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\BaseControl;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormRow
{

	/**
	 * @var array|FormColumn[]
	 */
	private $columns = [];

	/**
	 * @var string|NULL
	 */
	private $class = NULL;



	/**
	 * @param string|NULL $class
	 */
	public function __construct($class = NULL)
	{
		$this->class = $class;
	}



	/**
	 * @param mixed|BaseControl|FormRow $control
	 * @param string $type
	 * @return $this
	 */
	public function addColumn($control, $type = '')
	{
		$this->columns[] = new FormColumn($control, $type);
		return $this;
	}



	/**
	 * @return string
	 */
	public function getClass()
	{
		if (empty($this->class)) {
			return $this->getDefaultClass();
		}

		return $this->class;
	}



	/**
	 * @return string
	 */
	public function getDefaultClass()
	{
		return "columns-xs-min-" . count($this->columns);
	}



	/**
	 * @param BaseControl|FormRow $control
	 * @return bool
	 */
	public function hasControl($control)
	{
		foreach ($this->columns as $column) {

			if ($column->getControl() instanceof $control && $column->getControl()->name == $control->name) {
				return TRUE;
			}
		}

		return FALSE;
	}



	/**
	 * @return array|FormColumn[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

}



class FormRowException extends \RuntimeException {};
