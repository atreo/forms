<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\MultiSelectBox;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class EntityMultiSelectBox extends MultiSelectBox
{

	use IsEntityTrait;

	/**
	 * @var array
	 */
	private $items = [];

	/**
	 * @var string
	 */
	private $idKey;

	/**
	 * @var string
	 */
	private $nameKey;



	/**
	 *
	 * @param string $label
	 * @param array $items
	 * @param int $size
	 * @param string $idKey
	 * @param string $nameKey
	 */
	public function __construct($label = NULL, array $items = NULL, $size = NULL, $idKey = 'id', $nameKey = 'name')
	{
		$this->items = $items;
		$this->idKey = $idKey;
		$this->nameKey = $nameKey;

		parent::__construct($label, NULL, $size);

		if ($items !== NULL) {
			$this->setItems($items);
		}
	}



	public function setItems(array $items, $useKeys = TRUE)
	{
		$arrItems = [];

		foreach ($items as $key => $item) {
			if ($this->isEntity($item)) {
				$arrItems[$item->{'get' . $this->idKey}()] = $item->{'get' . $this->nameKey}();
			} else {
				$arrItems[$key] = $item;
			}
		}

		parent::setItems($arrItems);
	}



	private function prepareItems($value)
	{
		if ($value === NULL) {
			return [];
		}

		$arr = [];

		foreach ($value as $item) {
			if ($this->isEntity($item)) {
				$arr[] = $item->{'get' . $this->idKey}();
			} else {
				$arr[] = $item;
			}
		}

		return $arr;
	}



	public function setValue($value)
	{
		parent::setValue($this->prepareItems($value));
	}



	public function setDefaultValue($value)
	{
		$v = $this->prepareItems($value);
		parent::setDefaultValue($v);
	}



	public function getValue()
	{
		$back = debug_backtrace();
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "getControl" && $back[1]["class"] === "Nette\Forms\SelectBox") {
			return parent::getValue();
		}

		$keys = parent::getValue();
		$arr = [];

		foreach ($keys as $key) {
			foreach ($this->items as $item) {
				if ($item->{'get' . $this->idKey}() == $key) {
					$arr[] = $item;
				}
			}
		}

		return $arr;
	}

}
