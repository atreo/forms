<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\TextInput;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class DatePicker extends TextInput
{

	/**
	 * @var string
	 */
	private $format = "j.n.Y";

	/**
	 * @var string
	 */
	private $javascriptFormat = "d.m.yy";




	/**
	 * @param string $label
	 * @param int|NULL $maxLength
	 */
	public function __construct($label, $maxLength = NULL)
	{
		parent::__construct($label, $maxLength);
	}



	/**
	 * @param string $format
	 */
	public function setFormat($format)
	{
		$this->format = $format;
	}



	/**
	 * @return string
	 */
	public function getValue()
	{
		if ($this->value instanceof \DateTime) {
		} else {
			$value = NULL;
		}

		return $this->value;
	}



	/**
	 * @param string $value
	 * @return \Nette\Forms\Controls\TextBase
	 */
	public function setValue($value)
	{
		if ($value instanceof \DateTime) {
		} elseif (strlen($value)) {
			$value = new \DateTime(preg_replace('~([0-9]{4})-([0-9]{2})-([0-9]{2})~', '$3-$2-$1', $value));
		} else {
			$value = NULL;
		}

		$this->value = $value;
		$this->rawValue = $value ? $value->format($this->format) : NULL;
	}



	/**
	 * @return \Nette\Utils\Html
	 */
	public function getControl()
	{
		$control = parent::getControl();
		$control->addAttributes(['data-format' => $this->javascriptFormat]);
		$control->class = 'input input-datepicker';

		return $control;
	}

}
