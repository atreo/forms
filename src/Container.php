<?php

namespace Atreo\Forms;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Container extends \Nette\Forms\Container
{
	use FormFactories;
}
