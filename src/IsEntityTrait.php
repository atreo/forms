<?php

namespace Atreo\Forms;

use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
trait IsEntityTrait
{

	private function isEntity($item)
	{
		if (is_object($item)) {

			if (Strings::startsWith($item->getClassName(), 'Kdyby\GeneratedProxy')) {
				return TRUE;
			}

			$uses = class_uses($item);
			if ($this->hasUse($uses)) {
				return TRUE;
			}

			foreach (class_parents($item) as $parent) {
				$uses = class_uses($parent);
				if ($this->hasUse($uses)) {
					return TRUE;
				}
			}
		}

		return FALSE;
	}



	private function hasUse($uses)
	{
		$class1 = 'Kdyby\Doctrine\Entities\MagicAccessors';
		$class2 = 'Kdyby\Doctrine\Entities\Attributes\Identifier';

		return isset($uses[$class1]) || isset($uses[$class2]);
	}


}
