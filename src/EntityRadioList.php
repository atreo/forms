<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\RadioList;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class EntityRadioList extends RadioList
{

	use IsEntityTrait;

	/**
	 * @var array
	 */
	private $entities = [];

	/**
	 * @var string
	 */
	private $idKey;

	/**
	 * @var string
	 */
	private $nameKey;



	public function __construct($label = NULL, array $entities = NULL, $idKey = 'id', $nameKey = 'name')
	{
		$this->entities = $entities;
		$this->idKey = $idKey;
		$this->nameKey = $nameKey;

		parent::__construct($label);

		if ($entities !== NULL) {
			$this->setItems($entities);
		}
	}



	public function setItems(array $entities, $useKeys = TRUE)
	{
		$arrItems = [];

		foreach ($entities as $key => $item) {
			if ($this->isEntity($item)) {
				$arrItems[$item->{'get' . $this->idKey}()] = $item->{'get' . $this->nameKey}();
			} else {
				$arrItems[$key] = $item;
			}
		}

		parent::setItems($arrItems);
	}



	public function setValue($value)
	{
		if ($this->isEntity($value)) {
			$value = $value->{'get' . $this->idKey}();
		}
		parent::setValue($value);
	}



	public function getValue()
	{
		$back = debug_backtrace();
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "getControl" && $back[1]["class"] === "Nette\Forms\Controls\RadioList") {
			return parent::getValue();
		}
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "validateEqual" && $back[1]["class"] === "Nette\Forms\Controls\BaseControl") {
			return parent::getValue();
		}
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "validateEqual" && $back[1]["class"] === "Nette\Forms\Validator") {
			return parent::getValue();
		}
		$val = parent::getValue();
		foreach ($this->entities as $item) {
			if ($item->{'get' . $this->idKey}() == $val) {
				return $item;
			}
		}
		return NULL;
	}

}
