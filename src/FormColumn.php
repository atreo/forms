<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\BaseControl;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormColumn
{

	/**
	 * @var BaseControl
	 */
	private $control;

	/**
	 * @var string
	 */
	private $type = '';



	/**
	 * @param BaseControl|FormRow $control
	 * @param string $type
	 */
	public function __construct($control, $type = '')
	{
		if ($control instanceof BaseControl || $control instanceof FormRow) {} else {
			throw new FormRowException('Add instance of BaseControl or FormRow');
		}

		$this->control = $control;
		$this->type = $type;
	}



	/**
	 * @return BaseControl
	 */
	public function getControl()
	{
		return $this->control;
	}



	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

}
