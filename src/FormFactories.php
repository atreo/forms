<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextArea;
use Nette\Forms\Controls\TextInput;
use Kdyby\Doctrine\ResultSet;
use Nette\Forms\Controls\UploadControl;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
trait FormFactories
{

	/**
	 * @var array|FormRow[]
	 */
	protected $rows = [];



	/**
	 * @param string|NULL $class
	 * @return FormRow
	 */
	public function addRow($class = NULL)
	{
		return $this->rows[] = new FormRow($class);
	}



	/**
	 * @return array|FormRow[]
	 */
	public function getRows()
	{
		return $this->rows;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param int $size number of rows that should be visible
	 * @return \Nette\Forms\Controls\SelectBox
	 */
	public function addSelect($name, $label = NULL, array $items = NULL, $size = NULL)
	{
		$control = new SelectBox($label, $items, $size);
		$control->getControlPrototype()->class[] = 'select';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param int $size number of rows that should be visible
	 * @return \Nette\Forms\Controls\SelectBox
	 */
	public function addMultiSelect($name, $label = NULL, array $items = NULL, $size = NULL)
	{
		$control = new MultiSelectBox($label, $items, $size);
		$control->getControlPrototype()->class[] = 'input';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param string $idKey key of entity id
	 * @param string $nameKey key of entity name
	 * @return EntitySelectBox
	 * @throws \InvalidArgumentException
	 */
	public function addEntitySelect($name, $label = NULL, $items = NULL, $idKey = 'id', $nameKey = 'name')
	{
		if ($items instanceof ResultSet) {
			$items = $items->getIterator()->getArrayCopy();
		} elseif (!is_array($items)) {
			throw new \InvalidArgumentException("Items must be array or \Kdyby\Doctrine\ResultSet.");
		}

		$control = new EntitySelectBox($label, $items, $idKey, $nameKey);
		$control->getControlPrototype()->class[] = 'select';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param int $size number of rows that should be visible
	 * @param string $idKey key of entity id
	 * @param string $nameKey key of entity name
	 * @return EntitySelectBox
	 * @throws \InvalidArgumentException
	 */
	public function addEntityMultiSelect($name, $label = NULL, $items = NULL, $size = NULL, $idKey = 'id', $nameKey = 'name')
	{
		if ($items instanceof ResultSet) {
			$items = $items->getIterator()->getArrayCopy();
		} elseif (!is_array($items)) {
			throw new \InvalidArgumentException("Items must be array or \Kdyby\Doctrine\ResultSet.");
		}

		$control = new EntityMultiSelectBox($label, $items, $size, $idKey, $nameKey);
		$control->getControlPrototype()->class[] = 'select';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param int $cols width of the control
	 * @param int $maxLength maximum number of characters the user may enter
	 * @return \Nette\Forms\Controls\TextInput
	 */
	public function addText($name, $label = NULL, $cols = NULL, $maxLength = NULL)
	{
		$control = new TextInput($label, $cols, $maxLength);
		$control->getControlPrototype()->class[] = 'input';
		return $this[$name] = $control;
	}



	/**
	 * Adds multi-line text input control to the form.
	 * @param string $name control name
	 * @param string $label label
	 * @param int $cols width of the control
	 * @param int $rows height of the control in text lines
	 * @return \Nette\Forms\Controls\TextArea
	 */
	public function addTextArea($name, $label = NULL, $cols = 40, $rows = 10)
	{
		$control = new TextArea($label, $cols, $rows);
		$control->getControlPrototype()->class[] = 'input';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param int $cols width of the control
	 * @param int $maxLength maximum number of characters the user may enter
	 * @return \Nette\Forms\Controls\TextInput
	 */
	public function addPassword($name, $label = NULL, $cols = NULL, $maxLength = NULL)
	{
		$control = new TextInput($label, $cols, $maxLength);
		$control->setType('password');
		$control->getControlPrototype()->class[] = 'input';
		return $this[$name] = $control;
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param string $idKey key of entity id
	 * @param string $nameKey key of entity name
	 * @return EntitySelectBox
	 * @throws \InvalidArgumentException
	 */
	public function addEntityRadioList($name, $label = NULL, $items = NULL, $idKey = 'id', $nameKey = 'name')
	{
		if ($items instanceof ResultSet) {
			$items = $items->getIterator()->getArrayCopy();
		} elseif (!is_array($items)) {
			throw new \InvalidArgumentException("Items must be array or \Kdyby\Doctrine\ResultSet.");
		}

		return $this[$name] = new EntityRadioList($label, $items, $idKey, $nameKey);
	}



	/**
	 * @param string $name control name
	 * @param string $label label
	 * @param array $items items from which to choose
	 * @param string $idKey key of entity id
	 * @param string $nameKey key of entity name
	 * @return EntitySelectBox
	 * @throws \InvalidArgumentException
	 */
	public function addEntityCheckboxList($name, $label = NULL, $items = NULL, $idKey = 'id', $nameKey = 'name')
	{
		if ($items instanceof ResultSet) {
			$items = $items->getIterator()->getArrayCopy();
		} elseif (!is_array($items)) {
			throw new \InvalidArgumentException("Items must be array or \Kdyby\Doctrine\ResultSet.");
		}

		return $this[$name] = new EntityCheckboxList($label, $items, $idKey, $nameKey);
	}



	/**
	 * @param string $name
	 * @param string $label
	 * @return DatePicker
	 */
	public function addDatePicker($name, $label = NULL)
	{
		return $this[$name] = new DatePicker($label);
	}



	/**
	 * @param string $name
	 * @param string $label
	 * @param string|NULL $deleteLink
	 * @param string|NULL $path Path in wwwDir for example: uploads/folder/subFolder
	 * @param string|NULL $inputName input by which will be image named
	 *
	 * @return \Atreo\Forms\DatePicker
	 */
	public function addImageUpload($name, $label = NULL, $deleteLink = NULL, $path = NULL, $inputName = NULL)
	{
		$upload = new UploadControl($label, FALSE);
		$upload->addCondition(Form::FILLED)
			->addRule(Form::IMAGE);

		$upload->setOption('type', 'image');
		$upload->setOption('deleteLink', $deleteLink);
		$upload->setOption('path', $path);
		$upload->setOption('inputName', $inputName);

		return $this[$name] = $upload;
	}



	/**
	 * @param string $name
	 * @param string $label
	 * @param integer $selectLimit
	 * @return \Nette\Forms\Controls\TextInput
	 */
	public function addImagesSelector($name, $label = NULL, $selectLimit = 1)
	{
		$upload = new TextInput($label);
		$upload->setOption('maskMode', 'image');
		$upload->setOption('selectLimit', $selectLimit);
		return $this[$name] = $upload;
	}



	/**
	 * @param string $name
	 * @param string $label
	 * @param integer $selectLimit
	 * @return \Nette\Forms\Controls\TextInput
	 */
	public function addDocumentsSelector($name, $label = NULL, $selectLimit = 1)
	{
		$upload = new TextInput($label);
		$upload->setOption('maskMode', 'off');
		$upload->setOption('selectLimit', $selectLimit);
		return $this[$name] = $upload;
	}

}
