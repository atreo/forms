<?php

namespace Atreo\Forms;

use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Nette\DI\MissingServiceException;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Controls\Button;
use Nette\Forms\Controls\UploadControl;
use Nette\Http\FileUpload;
use Nette\Utils\Arrays;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Form extends \Nette\Application\UI\Form
{
	use FormFactories;

	const ANTI_SPAM_INPUT_NAME = 'user_account_url';

	/**
	 * @var mixed
	 */
	protected $entity = FALSE;

	/**
	 * @var mixed
	 */
	protected $isAntispamEnabled = FALSE;

	/**
	 * @var array
	 */
	protected $entityDefaults = [];

	/**
	 * @var array
	 */
	protected $customMappings = [];

	/**
	 * @var FileSaver
	 */
	protected $fileSaver;



	protected function attached($presenter)
	{
		parent::attached($presenter);

		try {
			$this->setTranslator($this->getPresenter()->context->getByType('Atreo\Translator\Translator'));
		} catch (MissingServiceException $e) {}

		if ($this->isAntispamEnabled) {
			$this->addText(self::ANTI_SPAM_INPUT_NAME, 'Url (prosím nevyplňujte)');
			$this->onValidate[] = function(Form $form) {
				if (!empty($form->values[self::ANTI_SPAM_INPUT_NAME])) {
					$this->addError('Prosím tento prvek nevyplňujte');
					return;
				}
			};
		}

		if (!isset($this['submit'])) {
			$this->addSubmit('submit', 'Uložit');
		}

		$this->mapToRows();
		$this->checkRows();

		/** @var FormRenderer $formRenderer */
		$formRenderer = $presenter->context->getByType(FormRenderer::class);
		$this->setRenderer($formRenderer);

		$this->fileSaver = $presenter->context->getByType(FileSaver::class);
	}



	/**
	 * @param array $defaults
	 */
	public function setEntityDefaults(array $defaults)
	{
		$this->entityDefaults = $defaults;
	}



	public function enableAntiSpam($enable = TRUE)
	{
		$this->isAntispamEnabled = $enable;
	}



	/**
	 * @param \Atreo\Forms\FileSaver $fileSaver
	 */
	public function setFileSaver(FileSaver $fileSaver)
	{
		$this->fileSaver = $fileSaver;
	}



	/**
	 * @param NULL $entity
	 */
	public function mapToEntity($entity = NULL)
	{
		if ($entity === NULL) {
			return;
		}

		$this->entity = $entity;
		$this->setDefaultsFromEntity();

		$this->onSuccess[] = $this->saveValuesToEntity;
	}



	/**
	 * @param string $key
	 * @param $callback
	 */
	public function setCustomMapping($key, $callback)
	{
		$this->customMappings[$key] = $callback;
	}



	/********************* loading defaults *********************/



	protected function setDefaultsFromEntity()
	{
		if (!$this->entity) {
			return;
		}

		$defaultValues = [];
		foreach ($this->getEditableComponents() as $component) {

			if (isset($component->control->type) && $component->control->type == 'password') {
				continue;
			}

			$defaultValues[$component->name] = Arrays::get($this->entityDefaults, $component->name, NULL);

			if ($defaultValue = $this->getDefaultFromEntity($component)) {
				if ($defaultValue instanceof ReadOnlyCollectionWrapper) {
					if (count($defaultValue)) {
						$defaultValues[$component->name] = $defaultValue;
					}
				} else {
					$defaultValues[$component->name] = $defaultValue;
				}
			}

			if ($component instanceof UploadControl) {
				$component->setOption('value', $defaultValues[$component->name]);
			}
		}

		$this->setDefaults($defaultValues);
	}



	/**
	 * @param BaseControl $component
	 * @return mixed
	 */
	protected function getDefaultFromEntity(BaseControl $component)
	{
		if (isset($this->entity->{$component->name})) {
			return $this->entity->{$component->name};
		}

		return NULL;
	}



	/********************* saving *********************/



	public function saveValuesToEntity()
	{
		if (!$this->entity) {
			return;
		}

		foreach ($this->values as $key => $value) {
			$this->saveValueToEntity($key, $value);
		}
	}



	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public function saveValueToEntity($key, $value)
	{
		if (isset($this->customMappings[$key])) {
			$this->customMappings[$key]($value);
		} elseif ($value instanceof FileUpload) {

			/** @var UploadControl $uc */
			$uploadControl = $this[$key];
			if ($uploadControl->getOption('type') != 'image') {
				return;
			}

			if (!$value->isOk()) {
				return;
			}

			if ($this->fileSaver === NULL) {
				throw new FormException('No $fileSaver instance and no customMappings.');
			}

			try {

				$fileName = NULL;
				if ($inputName = $uploadControl->getOption('inputName', NULL)) {
					$fileName = isset($this->values->$inputName) ? $this->values->$inputName : NULL;
				}

				if ($fileName) {
					$this->fileSaver->setFileName($fileName, $fileName ? TRUE : FALSE);
				}

				$this->fileSaver->setPath($uploadControl->getOption('path', NULL), $fileName);
				$this->entity->$key = $this->fileSaver->saveFromFileUpload($value);
			} catch (UnableToSaveFileException $e) {
				$this[$key]->addError("Soubor {$value->getName()} se nepodařilo uložit");
				return;
			}

		} elseif (isset($this->entity->$key) && (is_array($this->entity->$key) || $this->entity->$key instanceof ReadOnlyCollectionWrapper) && is_array($value)) {
			$collectionKey = $this->getCollectionKey($key);

			foreach ($this->entity->$key as $k => $v) {
				$this->entity->{'remove' . $collectionKey}($v);
			}

			foreach ($value as $k => $v) {
				if (!$this->entity->{'has' . $collectionKey}($v)) {
					$this->entity->{'add' . $collectionKey}($v);
				}
			}
		} else {
			$this->entity->$key = $value;
		}
	}



	/********************* other *********************/


	/**
	 * @param string $key
	 * @return string
	 */
	protected function getCollectionKey($key)
	{
		// categories, plans
		// category,   plan

		$collectionKey = NULL;
		if (substr($key, -3) === 'ies') {
			$collectionKey = substr($key, 0, -3) . 'y';
		} elseif (substr($key, -1) === 's') {
			$collectionKey = substr($key, 0, -1);
		}

		if ($collectionKey == NULL) {
			throw new FormException("Collection key for '{$key}' not found.");
		}

		return Strings::firstUpper($collectionKey);
	}



	/**
	 * @return array|BaseControl[]
	 */
	public function getEditableComponents()
	{
		$components = [];
		foreach ($this->components as $component) {
			if ($component instanceof Button) {} elseif ($component->name == "do") {} else {
				$components[] = $component;
			}
		}
		return $components;
	}



	/********************* rows *********************/


	public function mapToRows() {}



	public function checkRows()
	{
		if (count($this->rows) == 0) {
			return;
		}

		foreach ($this->getEditableComponents() as $component) {

			$found = FALSE;
			foreach ($this->rows as $row) {

				if ($row->hasControl($component)) {
					$found = TRUE;
				}
			}

			if (!$found) {
				throw new FormException("Control '{$component->name}' has no row!");
			}
		}
	}

}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormException extends \RuntimeException {}
