<?php

namespace Atreo\Forms\DI;

use Nette\DI\CompilerExtension;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormsExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('formRenderer'))
			->setClass('Atreo\Forms\FormRenderer');

		$builder->addDefinition($this->prefix('fileSaver'))
			->setClass('Atreo\Forms\FileSaver', [$builder->parameters['wwwDir']]);
	}

}
