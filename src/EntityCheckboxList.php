<?php

namespace Atreo\Forms;

use Nette\Forms\Controls\CheckboxList;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class EntityCheckboxList extends CheckboxList
{

	use IsEntityTrait;

	/**
	 * @var array
	 */
	private $entities = [];

	/**
	 * @var string
	 */
	private $idKey;

	/**
	 * @var string
	 */
	private $nameKey;



	public function __construct($label = NULL, array $entities = NULL, $idKey = 'id', $nameKey = 'name')
	{
		$this->entities = $entities;
		$this->idKey = $idKey;
		$this->nameKey = $nameKey;

		parent::__construct($label);

		if ($entities !== NULL) {
			$this->setItems($entities);
		}
	}



	public function setItems(array $items, $useKeys = TRUE)
	{
		$arrItems = [];

		foreach ($items as $key => $item) {
			if ($this->isEntity($item)) {
				$arrItems[$item->{'get' . $this->idKey}()] = $item->{'get' . $this->nameKey}();
 			} else {
				$arrItems[$key] = $item;
			}
		}

		parent::setItems($arrItems);
	}



	private function prepareItems($value)
	{
		if ($value === NULL) {
			return [];
		}

		$arr = [];

		foreach ($value as $item) {
			if ($this->isEntity($item)) {
				$arr[] = $item->{'get' . $this->idKey}();
			} else {
				$arr[] = $item;
			}
		}

		return $arr;
	}



	public function setValue($value)
	{
		parent::setValue($this->prepareItems($value));
	}



	public function setDefaultValue($value)
	{
		$v = $this->prepareItems($value);
		parent::setDefaultValue($v);
	}



	public function getValue()
	{
		$back = debug_backtrace();
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "getControl" && $back[1]["class"] === "Nette\Forms\Controls\CheckboxList") {
			return parent::getValue();
		}
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "validateEqual" && $back[1]["class"] === "Nette\Forms\Controls\BaseControl") {
			return parent::getValue();
		}
		if (isset($back[1]["function"]) && isset($back[1]["class"]) && $back[1]["function"] === "validateEqual" && $back[1]["class"] === "Nette\Forms\Validator") {
			return parent::getValue();
		}

		$keys = parent::getValue();
		$arr = [];

		foreach ($keys as $key) {
			foreach ($this->entities as $item) {
				if ($item->{'get' . $this->idKey}() == $key) {
					$arr[] = $item;
				}
			}
		}

		return $arr;
	}
}
