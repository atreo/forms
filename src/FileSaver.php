<?php

namespace Atreo\Forms;

use Nette\Http\FileUpload;
use Nette\InvalidStateException;
use Nette\Object;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FileSaver extends Object
{

	/**
	 * @var string
	 */
	private $wwwDir;

	/**
	 * Path in uploads dir. For example: subFolder/subSubFolder
	 * @var string|NULL
	 */
	private $path;

	/**
	 * @var string|NULL
	 */
	private $fileName;

	/**
	 * @var bool
	 */
	private $replaceIfExists = FALSE;



	/**
	 * @param string $wwwDir
	 */
	public function __construct($wwwDir)
	{
		$this->wwwDir = $wwwDir;
	}



	/**
	 * @param string|NULL $path
	 */
	public function setPath($path = NULL)
	{
		$this->path = $path;
	}



	/**
	 * @param string|NULL $fileName
	 * @param bool $replaceIfExists
	 */
	public function setFileName($fileName = NULL, $replaceIfExists = FALSE)
	{
		$this->fileName = $fileName;
		$this->replaceIfExists = $replaceIfExists;
	}



	/**
	 * @param FileUpload $file
	 *
	 * @return string Path in uploads dir for example: subFolder/image.png
	 * @throws UnableToSaveFileException
	 */
	public function saveFromFileUpload(FileUpload $file)
	{
		$this->checkBasePathExists();

		$try = 0;
		do {
			$fileName = $this->constructFileName($file->sanitizedName, $try);
			$filePath = $this->getBasePath() . '/' . $fileName;
			$try++;
		} while (!$this->replaceIfExists && file_exists($filePath));


		try {
			$file->move($filePath);
			return $this->path . '/' . $fileName;
		} catch (InvalidStateException $e) {
			throw new UnableToSaveFileException($e->getMessage(), $e->getCode(), $e);
		}
	}



	/**
	 * @param string $originalName
	 * @param int $try
	 *
	 * @return string
	 */
	protected function constructFileName($originalName, $try = 0)
	{
		$name = Strings::lower(pathinfo($originalName, PATHINFO_FILENAME));
		$extension = Strings::lower(pathinfo($originalName, PATHINFO_EXTENSION));

		$fileName = $this->fileName ? $this->fileName : $name;
		$fileName = Strings::webalize($fileName);

		return $fileName . ($try > 0 ? '-' . $try : '') . '.' . $extension;
	}



	/**
	 * @return string
	 */
	protected function getBasePath()
	{
		return $this->wwwDir . ($this->path ? ('/' . $this->path) : '');
	}



	protected function checkBasePathExists()
	{
		if (!file_exists($this->getBasePath())) {
			mkdir($this->getBasePath(), 0777, TRUE);
		}
	}

}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class UnableToSaveFileException extends \Exception {}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class ImageException extends \Exception {}
