<?php

namespace Atreo\Forms;

use Atreo\UI\TemplateGenerator;
use Nette\Forms\Rendering\DefaultFormRenderer;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormRenderer extends DefaultFormRenderer
{

	/**
	 * @var TemplateGenerator
	 */
	private $templateGenerator;



	/**
	 * @param TemplateGenerator $templateGenerator
	 */
	public function __construct(TemplateGenerator $templateGenerator)
	{
		$this->templateGenerator = $templateGenerator;
	}



	public function render(\Nette\Forms\Form $form)
	{
		$template = $this->templateGenerator->generate();
		$template->form = $form;

		if ($form instanceof Form && count($form->getRows())) {
			$template->setFile(__DIR__ . '/Templates/Forms/rowForm.latte');
		} else {
			$template->setFile(__DIR__ . '/Templates/Forms/plainForm.latte');
		}

		$template->render();
	}



	public function renderPair(\Nette\Forms\IControl $control, $params = [])
	{
		foreach ($params as $key => $value) {
			$control->setOption($key, $value);
		}

		$template = $this->templateGenerator->generate();
		$template->setFile(__DIR__ . '/Templates/Forms/@partials/component.latte');
		$template->component = $control;

		$template->render();
	}

}
