<?php

namespace Atreo\Forms;

use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FormMacros extends MacroSet
{

	/**
	 * @param Compiler $compiler
	 * @return MacroSet
	 */
	public static function install(Compiler $compiler)
	{
		$me = new static($compiler);
		$me->addMacro('pair', array($me, 'macroPair'));
		return $me;
	}



	/**
	 * @param MacroNode $node
	 * @param PhpWriter $writer
	 * @return string
	 */
	public function macroPair(MacroNode $node, PhpWriter $writer)
	{
		return $writer->write('echo $form->getRenderer()->renderPair($_form[%node.word], %node.array)');
	}

}
